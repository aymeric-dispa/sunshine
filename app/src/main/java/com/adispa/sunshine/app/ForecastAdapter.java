package com.adispa.sunshine.app;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import static com.adispa.sunshine.app.ForecastFragment.COL_WEATHER_CONDITION_ID;
import static com.adispa.sunshine.app.ForecastFragment.COL_WEATHER_DATE;
import static com.adispa.sunshine.app.ForecastFragment.COL_WEATHER_DESC;
import static com.adispa.sunshine.app.ForecastFragment.COL_WEATHER_MAX_TEMP;
import static com.adispa.sunshine.app.ForecastFragment.COL_WEATHER_MIN_TEMP;
/**
 * {@link ForecastAdapter} exposes a list of weather forecasts
 * from a {@link android.database.Cursor} to a {@link android.widget.ListView}.
 */
public class ForecastAdapter extends CursorAdapter {
    private final int VIEW_TYPE_TODAY = 0;
    private final int VIEW_TYPE_FUTURE_DAY = 1;
    private boolean mUseTodayLayout=true;

    public ForecastAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    public void setUseTodayLayout(boolean mUseTodayLayout) {
        this.mUseTodayLayout = mUseTodayLayout;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {

        return (position == 0 && mUseTodayLayout)  ? VIEW_TYPE_TODAY : VIEW_TYPE_FUTURE_DAY;
    }


    /*
        Remember that these views are reused as needed.
     */
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        int viewType = getItemViewType(cursor.getPosition());


        int layoutId = viewType == VIEW_TYPE_TODAY ? R.layout.list_item_forecast_today : R.layout.list_item_forecast;
        View view = LayoutInflater.from(context).inflate(layoutId, parent, false);


        ViewHolder viewHolder = new ViewHolder(view);
        view.setTag(viewHolder);
        return view;
    }

    /*
        This is where we fill-in the views with the contents of the cursor.
     */
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // our view is pretty simple here --- just a text view
        // we'll keep the UI functional with a simple (and slow!) binding.
        int viewType = getItemViewType(cursor.getPosition());
        int weatherCondId = cursor.getInt(COL_WEATHER_CONDITION_ID);

        boolean isMetric = Utility.isMetric(context);
        long dateInMillis = cursor.getLong(COL_WEATHER_DATE);
        String highTemp = Utility.formatTemperature(context, cursor.getFloat(COL_WEATHER_MAX_TEMP), isMetric);
        String lowTemp = Utility.formatTemperature(context, cursor.getFloat(COL_WEATHER_MIN_TEMP), isMetric);
        String description = cursor.getString(COL_WEATHER_DESC);
        ViewHolder viewHolder = (ViewHolder) view.getTag();

        if (viewType == VIEW_TYPE_TODAY) {
            viewHolder.iconView.setImageResource(Utility.getArtResourceForWeatherCondition(weatherCondId));
        } else {
            viewHolder.iconView.setImageResource(Utility.getIconResourceForWeatherCondition(weatherCondId));
        }
        viewHolder.lowTempView.setText(lowTemp);
        viewHolder.highTempView.setText(highTemp);
        viewHolder.descriptionView.setText(description);
        viewHolder.iconView.setContentDescription(description);
        viewHolder.dateView.setText(Utility.getFriendlyDayString(context, dateInMillis));


    }

    //Cache of the children views.
    private static class ViewHolder {
        static int d = 5;
        public final ImageView iconView;
        public final TextView dateView;
        public final TextView descriptionView;
        public final TextView highTempView;
        public final TextView lowTempView;

        public ViewHolder(View view) {
            iconView = (ImageView) view.findViewById(R.id.list_item_icon);
            lowTempView = (TextView) view.findViewById(R.id.list_item_low_textview);
            highTempView = (TextView) view.findViewById(R.id.list_item_high_textview);
            dateView = (TextView) view.findViewById(R.id.list_item_date_textview);
            descriptionView = (TextView) view.findViewById(R.id.list_item_forecast_textview);
        }

    }
}